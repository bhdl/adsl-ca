package arteries.hu.adslca.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import arteries.hu.adslca.R;
import arteries.hu.adslca.Utils.Utils;
import arteries.hu.adslca.managers.AlarmManager;


/**
 * My customized alert
 */
public class MyAlarm {

    private static TextView tv_title;
    private static TextView tv_message;
    private static TextView tv_ok;

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    public static void create(Context context, String message, final Runnable runnable) {

        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        final Dialog dialog = adb.create();
        dialog.show();

        //Settings
        dialog.setContentView(R.layout.dialog_scs_alarm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setLayout((int) (Utils.getScrWidth(context) * 0.8f), (int) (Utils.getScrHeight(context) * 0.8f));
        dialog.setCancelable(false);

        tv_title = dialog.findViewById(R.id.scs_alarm_tv_title);
        tv_message = dialog.findViewById(R.id.scs_alarm_tv_message);

        tv_ok = dialog.findViewById(R.id.scs_alarm_tv_ok);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (runnable != null) {
                    runnable.run();
                }
                dialog.cancel();
            }
        });

        tv_message.setText(message);

    }
}
