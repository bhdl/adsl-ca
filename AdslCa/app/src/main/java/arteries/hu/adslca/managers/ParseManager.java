package arteries.hu.adslca.managers;

import com.google.gson.Gson;

/**
 * Created by scsaba on 2018.09.20..
 * Manager to handle json objects and xml strings
 */

public class ParseManager{

    /**
     * From Json string to object
     * @param jsonString
     * @param cls
     * @return
     */
    public static Object jsonToObject(String jsonString, Class cls){
        Gson gson = new Gson();
        return gson.fromJson(jsonString, cls);
    }
// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Object to Json
     * @param object
     * @return
     */
    public static String objectToJson(Object object){

        String ret = null;

        try {
            Gson gson = new Gson();
            ret = gson.toJson(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

}
