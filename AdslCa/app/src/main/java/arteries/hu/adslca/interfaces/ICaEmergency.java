package arteries.hu.adslca.interfaces;

import arteries.hu.adslca.models.CaEmergency;

public interface ICaEmergency {
    public void onCaEmergency(CaEmergency caEmergency);
}
