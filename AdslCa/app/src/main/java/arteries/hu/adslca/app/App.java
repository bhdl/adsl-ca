package arteries.hu.adslca.app;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;

import java.net.URISyntaxException;

import arteries.hu.adslca.managers.SocketManager;

public class App extends Application{

    public static Context appContext;
    public static String socket_url = "http://socket_url:6500/collision-avoidance";
    public static MediaPlayer mediaPlayer = new MediaPlayer();

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;

        // Init
        try {
            SocketManager.getInstance();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
