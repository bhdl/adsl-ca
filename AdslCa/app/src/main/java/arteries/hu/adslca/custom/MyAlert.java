package arteries.hu.adslca.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import arteries.hu.adslca.R;
import arteries.hu.adslca.Utils.Utils;


/**
 * My customized alert
 */
public class MyAlert {

    private static TextView tv_title;
    private static TextView tv_message;
    private static TextView tv_ok;

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    public static void create(
              Context context,
              String title,
              String message,
              int waitInMsec) {

        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        final Dialog dialog = adb.create();
        dialog.show();

        //Settings
        dialog.setContentView(R.layout.dialog_scs_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setLayout((int) (Utils.getScrWidth(context) * 0.8f), RelativeLayout.LayoutParams.WRAP_CONTENT);

        tv_title = (TextView) dialog.findViewById(R.id.xml_scs_alert_tv_title);
        tv_message = (TextView) dialog.findViewById(R.id.xml_scs_alert_tv_message);

        tv_ok = (TextView) dialog.findViewById(R.id.xml_scs_alert_tv_ok);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        //Settings
        if (title.equals("") != true) {
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(title);
        } else {
            tv_title.setVisibility(View.GONE);
        }

        tv_title.setText(title);
        tv_message.setText(message);
        tv_ok.setText(context.getString(R.string.general_ok));

        if (waitInMsec != 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, waitInMsec);
        }
    }
}
