package arteries.hu.adslca.managers;

import android.content.SharedPreferences;

import arteries.hu.adslca.app.App;

import static android.content.Context.MODE_PRIVATE;

/**
 * MAnager for shared preferences
 */
public class PrefManager {

    public static String KEY_TAG_ID = "tag_id";

    private final String PREF_NAME = "adsl_ca_prefs";

    private static PrefManager instance;
    private SharedPreferences prefs;

    public static PrefManager getInstance(){
        if (instance == null) {
            instance = new PrefManager();
        }
        return instance;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Get shared preference String value
     * @param key
     * @return
     */
    public String getSharedPreference(String key) {

        String ret = null;

        if (prefs == null) {
            prefs = App.appContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        }
        ret = prefs.getString(key, null);

        return ret;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Set Shared Preferences
     * @param key
     * @param val
     */
    public void setSharedPreference(String key, String val){
        if (prefs == null) {
            prefs = App.appContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        }

        SharedPreferences.Editor sharedEditor = prefs.edit();
        sharedEditor.putString(key, val);
        sharedEditor.apply();
    }

}
