package arteries.hu.adslca.managers;

import java.net.URI;
import java.net.URISyntaxException;

import arteries.hu.adslca.Utils.Utils;
import arteries.hu.adslca.app.App;
import arteries.hu.adslca.interfaces.ICaEmergency;
import arteries.hu.adslca.models.CaEmergency;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Socket.io
 */
public class SocketManager {

    private static SocketManager instance = null;
    public static boolean connected = false;
    public static Socket socket;
    private static ICaEmergency iCaEmergency;

    public static SocketManager getInstance() throws URISyntaxException {
        if(instance == null) {
            instance = new SocketManager();
        }
        return instance;
    }

    public static boolean cnd(){
        return socket.connected();
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @throws URISyntaxException
     */
    private SocketManager() throws URISyntaxException {
        IO.Options socketOptions = new IO.Options();
        socketOptions = new IO.Options();
        //socketOptions.secure = true;
        socketOptions.reconnection = true;

        Utils.log("Socket connection started!");

        //----------Front end socket----------
        String socketUrl = App.socket_url;
        URI uri = new URI(socketUrl);
        socket = IO.socket(uri, socketOptions);
        socket.connect();
        socket.on("connect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                connected = true;
                Utils.log("SOCKET:  " + "Frontend socket CONNECTED");
            }
        });
        socket.on("disconnecting", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                connected = false;
                Utils.log("SOCKET:  " + "Socket DISCONNECTING");
            }
        });
        socket.on("disconnect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                connected = false;
                Utils.log("SOCKET:  " + "Socket DISCONNECT");
            }
        });
        socket.on("error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                connected = false;
                Utils.log("SOCKET:  " + "Socket ERROR");
            }
        });
    }

// *******************************************************************************************************************************************
// * EVENTS
// *******************************************************************************************************************************************
    private static Emitter.Listener listener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                Utils.log("**** RIASZTÁS ****: " + args[0]);
                String jsonString = args[0].toString();
                //iResponse.gotData(jsonString);
                CaEmergency caEmergency = (CaEmergency) ParseManager.jsonToObject(jsonString, CaEmergency.class);
                Utils.log("Just a stop");
                if (iCaEmergency != null) {
                    iCaEmergency.onCaEmergency(caEmergency);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    } ;

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    /**
     * Register listener dinamically
     * @param tagId
     */
    public static void registerListener(String tagId){
        if (tagId != null) {
            socket.on("ca-emergency-" + tagId, listener);
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Unregister listener
     * @param tagId
     */
    public static void unregisterListener(String tagId){
        socket.off("ca-emergency-" + tagId, listener);
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Set CaEmergency listener
     * @param _iCaEmergency
     */
    public static void setCaEmergencyListener(ICaEmergency _iCaEmergency){
        iCaEmergency = _iCaEmergency;
    }
}
