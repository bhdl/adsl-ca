package arteries.hu.adslca.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import arteries.hu.adslca.R;
import arteries.hu.adslca.Utils.Utils;

/**
 * Created by SCsaba on 15. 02. 10..
 */
public class MyDialog {

    private static TextView tv_title;
    private static TextView tv_message;
    private static TextView tv_ok;
    private static TextView tv_cancel;

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    public static void create(
          Context context,
          String title,
          String message,
          String ok_text,
          final Runnable ok_runnable,
          String cancel_text,
          final Runnable cancel_runnable){

        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        final Dialog dialog = adb.create();
        dialog.show();

        //Settings
        dialog.setContentView(R.layout.dialog_scs_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.getWindow().setDimAmount(0.8f);

        dialog.getWindow().setLayout((int) (Utils.getScrWidth(context) * 0.8f), RelativeLayout.LayoutParams.WRAP_CONTENT);

        tv_title = (TextView) dialog.findViewById(R.id.xml_scs_dialog_tv_title);
        tv_message = (TextView) dialog.findViewById(R.id.xml_scs_dialog_tv_message);

        tv_ok = (TextView) dialog.findViewById(R.id.xml_scs_dialog_tv_ok);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ok_runnable != null) {
                    ok_runnable.run();
                }
                dialog.cancel();
            }
        });

        tv_cancel = (TextView) dialog.findViewById(R.id.xml_scs_dialog_tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancel_runnable != null) {
                    cancel_runnable.run();
                }
                dialog.cancel();
            }
        });


        //Settings
        if (title.equals("") != true) {
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(title);
        } else {
            tv_title.setVisibility(View.GONE);
        }

        tv_message.setText(message);
        tv_ok.setText(ok_text);
        tv_cancel.setText(cancel_text);

    }
}
