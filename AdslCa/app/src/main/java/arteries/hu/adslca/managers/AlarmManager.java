package arteries.hu.adslca.managers;

import android.media.ToneGenerator;
import android.os.Vibrator;

import arteries.hu.adslca.Utils.Utils;
import arteries.hu.adslca.activities.MainActivity;
import arteries.hu.adslca.app.App;
import arteries.hu.adslca.custom.MyAlarm;
import arteries.hu.adslca.interfaces.ICaEmergency;
import arteries.hu.adslca.models.CaEmergency;

public class AlarmManager {

    private static boolean isAlarming = false;
    private static Vibrator vibrator;

    public static void startListening(){
        SocketManager.setCaEmergencyListener(iCaEmergency);
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Start vibration
     */
    private static void vibrate(){

        vibrator = (Vibrator) App.appContext.getSystemService(App.appContext.VIBRATOR_SERVICE);

        // Start without a delay
        // Vibrate for 100 milliseconds
        // Sleep for 1000 milliseconds
        long[] pattern = {0, 1000, 1000};

        // The '0' here means to repeat indefinitely
        // '0' is actually the index at which the pattern keeps repeating from (the start)
        // To repeat the pattern from any other point, you could increase the index, e.g. '1'
        vibrator.vibrate(pattern, 0);

    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Stop vibration
     */
    private static void stopVibration(){
        if (vibrator != null) {
            vibrator.cancel();
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Stop Alarm
     */
    public static void stopAlarm(){
        stopVibration();
        SoundManager.stop();
        isAlarming = false;
    }

// *******************************************************************************************************************************************
// * Implemented interfaces
// *******************************************************************************************************************************************
    private static ICaEmergency iCaEmergency = new ICaEmergency() {
    @Override
        public void onCaEmergency(final CaEmergency caEmergency) {
            if (!isAlarming) {
                isAlarming = true;
                SoundManager.play("alarm.mp3",true);
                vibrate();

                MainActivity.Companion.getAct().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MyAlarm.create(
                            MainActivity.Companion.getAct(),
                            String.format("Ütközés veszély!\n A %1s eszköz %2s méter távolságon belül van",
                                    caEmergency.data.emergency.sharer,
                                    Float.toString(caEmergency.data.emergency.distance)),
                            new Runnable() {
                                @Override
                                public void run() {
                                    AlarmManager.stopAlarm();
                                }
                            }
                        );
                    }
                });
                Utils.log("Just a stop");
            }
        }
    };

}
