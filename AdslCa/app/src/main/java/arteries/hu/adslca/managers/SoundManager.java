package arteries.hu.adslca.managers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;

import arteries.hu.adslca.app.App;

/**
 * PLAY SOUND
 * @author artmini
 *
 */


public class SoundManager{

	/**
	 * Play
	 * @param fileName
	 * @param isLoop
	 */
	public static void play(String fileName, boolean isLoop){
		new SoundPlayer(fileName, isLoop).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

// -----------------------------------------------------------------------------------------------------------------------------
	/**
	 * Stop
	 */
	public static void stop(){
		if (App.mediaPlayer != null) {

			if (App.mediaPlayer.isPlaying()) {
				App.mediaPlayer.stop();
			}

			App.mediaPlayer.release();
			App.mediaPlayer = null;
		}
	}

}

// *******************************************************************************************************************************************
// * Classes
// *******************************************************************************************************************************************

class SoundPlayer extends AsyncTask<Boolean, Boolean, Boolean> {

	private String fileName;
	private boolean isLoop;

	public SoundPlayer(String fileName, boolean isLoop){
		this.fileName = fileName;
		this.isLoop = isLoop;
	}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	@Override
	protected Boolean doInBackground(Boolean... params) {

		try {
			AudioManager audioManager = ((AudioManager) App.appContext.getSystemService(Context.AUDIO_SERVICE));
			audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);

			if (App.mediaPlayer != null) {
				App.mediaPlayer.release();
			}
			App.mediaPlayer = new MediaPlayer();
			AssetFileDescriptor descriptor;
			descriptor = App.appContext.getAssets().openFd( fileName );
			App.mediaPlayer.setDataSource( descriptor.getFileDescriptor(), descriptor.getStartOffset(),  descriptor.getLength() );
		    descriptor.close();
		    App.mediaPlayer.prepare();
			App.mediaPlayer.setVolume(1.0f, 1.0f);
		    App.mediaPlayer.start();
			App.mediaPlayer.setLooping(isLoop);
			App.mediaPlayer.setOnCompletionListener(playOnCompletionListener);
		    //App.mediaPlayer.setOnPreparedListener(playOnPreparedListener);
		    App.mediaPlayer.setOnErrorListener(mpOnErrorListener);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

// *******************************************************************************************************************************************
// * EVENTS	
// *******************************************************************************************************************************************
	private OnCompletionListener playOnCompletionListener = new OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			App.mediaPlayer.release();
			App.mediaPlayer = null;
		}
	};
	
// ------------------------------------------------------------------------------------------------------------------------------------------------------------
	private OnErrorListener mpOnErrorListener = new OnErrorListener() {
		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			return false;
		}
	};

}
