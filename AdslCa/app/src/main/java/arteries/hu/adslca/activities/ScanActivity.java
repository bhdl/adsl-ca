package arteries.hu.adslca.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import arteries.hu.adslca.R;
import arteries.hu.adslca.Utils.Utils;
import arteries.hu.adslca.managers.SoundManager;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScanActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    public static String MODE_ENTRY = "entry";
    public static String MODE_CATERING = "catering";

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        //mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
        setContentView(R.layout.activity_scanner);                // Set the scanner view as the content view
        mScannerView = (ZBarScannerView) findViewById(R.id.zbar_scanner_view);
        //TextView tv_title = (TextView) findViewById(R.id.scanner_tv_title);

//        try {
//            String mode = getIntent().getExtras().getString("mode");
//            if (mode.equals(MODE_ENTRY)) {
//                tv_title.setText(R.string.scanner_entry);
//            } else {
//                tv_title.setText(R.string.scanner_catering);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

// -----------------------------------------------------------------------------------------------------------------------------
    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

// -----------------------------------------------------------------------------------------------------------------------------
    @Override
    public void handleResult(me.dm7.barcodescanner.zbar.Result result) {
        // Do something with the result here

        SoundManager.play("beep_short.mp3", false);

        Intent returnIntent = new Intent();
        returnIntent.putExtra("qr_code", result.getContents());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }
}

