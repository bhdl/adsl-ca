package arteries.hu.adslca.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import arteries.hu.adslca.R
import arteries.hu.adslca.Utils.Utils
import arteries.hu.adslca.custom.MyAlert
import arteries.hu.adslca.custom.MyDialog
import arteries.hu.adslca.managers.AlarmManager
import arteries.hu.adslca.managers.PrefManager
import arteries.hu.adslca.managers.SocketManager
import arteries.hu.adslca.services.MySocketService
import android.os.PowerManager
import android.os.PowerManager.WakeLock



class MainActivity : AppCompatActivity() {

    private val MY_CAMERA_REQUEST_CODE = 1

    var tv_leval : TextView? = null
    var tv_tag_id : TextView? = null

    companion object {
        var act: Activity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        act = this;

        tv_tag_id = findViewById(R.id.main_tv_tag_id);
        tv_tag_id?.setText(PrefManager.getInstance().getSharedPreference(PrefManager.KEY_TAG_ID));

        tv_leval = findViewById(R.id.main_tv_tag_remove);
        tv_leval?.setOnClickListener {

            MyDialog.create(
                    this,
                    getString(R.string.deposition_title),
                    getString(R.string.deposition_message),
                    getString(R.string.general_yes),
                    {
                        SocketManager.unregisterListener(PrefManager.getInstance().getSharedPreference(PrefManager.KEY_TAG_ID))

                        tv_tag_id?.setText("")
                        PrefManager.getInstance().setSharedPreference(PrefManager.KEY_TAG_ID, "");
                        init()

                    },
                    getString(R.string.general_no),
                    null
            )

        }

        Utils.log("Activity started")
        val powerManager = getSystemService(POWER_SERVICE) as PowerManager
        val wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::MyWakelockTag")
        wakeLock.acquire()

        AlarmManager.startListening();



        init();

    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Init
     */
    private fun init(){

        val myVal = PrefManager.getInstance().getSharedPreference(PrefManager.KEY_TAG_ID)

        if (myVal == null || myVal.isEmpty()) {
            MyDialog.create(
                this,
                getString(R.string.no_scanned_tag),
                getString(R.string.title_scan_alert),
                getString(R.string.ca_scan),
                {
                    val intent = Intent(this, MySocketService::class.java)
                    startService(intent)
                    val tag_id : String? = PrefManager.getInstance().getSharedPreference(PrefManager.KEY_TAG_ID)
                    SocketManager.registerListener(tag_id)

                    if(checkPermissions()){
                        startScan()
                    }
                },
                getString(R.string.ca_exit),
                {
                    finish();
                }
            )
        } else {
            val intent = Intent(this, MySocketService::class.java)
            startService(intent)
            val tag_id : String? = PrefManager.getInstance().getSharedPreference(PrefManager.KEY_TAG_ID)
            SocketManager.registerListener(tag_id)
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Check permission
     */
    fun checkPermissions() : Boolean{
        var ret = false;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            requestPermissions((this as Activity), arrayOf(Manifest.permission.CAMERA), MY_CAMERA_REQUEST_CODE)
        } else {
            ret = true;
        }
        return ret
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Start QR Code scan
     */
    fun startScan() {
        val intent = Intent(this, ScanActivity::class.java)
        this.startActivityForResult(intent, MY_CAMERA_REQUEST_CODE)
    }

// *******************************************************************************************************************************************
// * SYSTEM
// *******************************************************************************************************************************************
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.ca_camera_enabled), Toast.LENGTH_LONG).show()
                startScan()
            } else {
                Toast.makeText(this, getString(R.string.ca_camera_declined), Toast.LENGTH_LONG).show()
                init()
            }
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * On Activity result
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK){
            when (requestCode) {
                MY_CAMERA_REQUEST_CODE -> {
                    if (data != null){
                        val tag_id : String? = data.getStringExtra("qr_code");
                        PrefManager.getInstance().setSharedPreference("tag_id", tag_id);
                        SocketManager.registerListener(tag_id)
                        tv_tag_id?.setText(tag_id)
                        MyAlert.create(
                            this,
                                getString(R.string.ca_scan_succes),
                            getString(R.string.ca_scan_status),
                            0
                        )
                    }
                }
            }
        } else {
            init()
        }
    }

}
