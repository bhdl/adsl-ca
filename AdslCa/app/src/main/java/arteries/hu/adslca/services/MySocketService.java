package arteries.hu.adslca.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.net.URISyntaxException;

import arteries.hu.adslca.Utils.Utils;
import arteries.hu.adslca.managers.SocketManager;

public class MySocketService extends IntentService {
    public MySocketService() {
        super("MySocketService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Utils.log("Service started");
        try {
            SocketManager socketManager = SocketManager.getInstance();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
