package arteries.hu.adslca.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;

import arteries.hu.adslca.app.App;
import arteries.hu.adslca.managers.SoundManager;

public class Utils {

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Is Online?
      * @return
     */
    public static boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)App.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();

            if (netInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     *
     * @param message
     */
    public static void log(String message){
        Log.i("ADSL-CA", message);
        //System.out.print(message);
    }

// *******************************************************************************************************************************************
// * SCREEN
// *******************************************************************************************************************************************
    /**
     * Get screen width
     * @return
     */
    public static int getScrWidth(Context context){
        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Get screen height
     * @return
     */
    public static int getScrHeight(Context context){
        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

}
